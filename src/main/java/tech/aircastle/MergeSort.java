package tech.aircastle;

import java.util.ArrayList;
import java.util.List;

public class MergeSort {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Usage: java -jar mergesort.jar [val1] [val2] ... [valN]");
            return;
        }

        List<Integer> items = new ArrayList<>();
        for (String arg : args) {
            Integer item = Integer.valueOf(arg);
            items.add(item);
        }
        System.out.println("items: " + items);

        List<Integer> itemsSorted = sort(items);
        System.out.println("itemsSorted: " + itemsSorted);
    }

    private static List<Integer> sort(List<Integer> items) {
        if (items.size() <= 1) {
            return items;
        }

        int middle = items.size() / 2;
        return merge(sort(items.subList(0, middle)), sort(items.subList(middle, items.size())));
    }

    private static List<Integer> merge(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();
        int indexA = 0;
        int indexB = 0;

        while (indexA < a.size() || indexB < b.size()) {
            if (indexA < a.size() && indexB < b.size()) {
                if (a.get(indexA) < b.get(indexB)) {
                    result.add(a.get(indexA));
                    indexA ++;
                } else {
                    result.add(b.get(indexB));
                    indexB ++;
                }
            } else if (indexA < a.size()) {
                result.add(a.get(indexA));
                indexA ++;
            } else {
                result.add(b.get(indexB));
                indexB ++;
            }
        }

        return result;
    }
}
