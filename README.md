Merge Sort Command Line Application
====
This is a simple command line application which sorts the integer parameters 
provided as command line arguments.

Usage: java -jar mergesort.jar [val1] [val2] ... [valN]

Output: sorted series of [var1] ... [varN]